#include <RcppArmadillo.h>
// [[Rcpp::depends(RcppArmadillo)]]

using namespace Rcpp;

// [[Rcpp::export]]
NumericMatrix rcpp_orient(NumericMatrix mat) {

    int m = mat.nrow(),
        n = mat.ncol();

    // Initialize count
    int count = 0;

    // Flip first row to have non-negative real part in first component
    if (mat(0,0) < 0) {
        mat.row(0) = -mat.row(0);
    }

    // Reorder successive observations based on Euclidean distance
    for (int i = 1; i < m; i++) {

        // Current and previous observations
        NumericVector v1 = mat.row(i);
        NumericVector v0 = mat.row(i-1);

        // Distances
        double dpos = 0;
        double dneg = 0;
        for(int j = 0; j < n; j++) {
            dpos += pow( v1(j) - v0(j), 2.0);
            dneg += pow(-v1(j) - v0(j), 2.0);
        }

        // Reorient
        if (dneg < dpos) {
            mat.row(i) = -mat.row(i);
        }

    }
    return mat;
}
