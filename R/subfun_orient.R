subfun_orient <- function(x) {

    # Sample only complete observations
    idx <- complete.cases(x)
    x.complete <- x[idx,]

    # Reorient
    x.oriented <- rcpp_orient(x.complete)
    x[idx,] <- x.oriented
    return(x)

}
