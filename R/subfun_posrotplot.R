subfun_plotPosition <- function(v, timestamps, label = '') {

    # Create plot object for ggplot
    n  <- length(timestamps)
    df <- data.frame(Time = timestamps,
                     Value = as.numeric(v),
                     Coordinate = rep(c('x', 'y', 'z'), each = n))

    p <- ggplot(df, aes(x = Time, y = Value, group = Coordinate,
                        color = Coordinate)) +
        geom_point(size = 2) + geom_path(size = 1) +
        theme_bw() + ggtitle(label) + ylab('') +
        scale_color_manual('', values = c('#a6cee3', '#1f78b4', '#b2df8a')) +
        theme(legend.position = 'bottom',
              legend.text = element_text(size = 14),
              axis.text = element_text(size = 14),
              axis.title = element_text(size = 16),
              title = element_text(size = 14))
    return(p)
}

subfun_plotRotation <- function(q, timestamps, label = '') {

    # Create plot object for ggplot
    n  <- length(timestamps)
    df <- data.frame(Time = timestamps,
                     Value = as.numeric(q),
                     Coordinate = rep(paste('q', 0:3, sep = ''), each = n))

    p <- ggplot(df, aes(x = Time, y = Value, group = Coordinate,
                        color = Coordinate)) +
        geom_point(size = 2) + geom_path(size = 1) +
        theme_bw() + ggtitle(label) + ylab('') +
        scale_color_manual('', values = c('#a6cee3', '#1f78b4', '#b2df8a', '#33a02c')) +
        theme(legend.position = 'bottom',
              legend.text = element_text(size = 14),
              axis.text = element_text(size = 14),
              axis.title = element_text(size = 16),
              title = element_text(size = 14))
    return(p)
}

subfun_plotDQ <- function(dq, timestamps, label = '') {

    # Create plot object for ggplot
    n  <- length(timestamps)
    df <- data.frame(Time = timestamps,
                     Value = as.numeric(dq),
                     Coordinate = rep(paste('dq', 0:7, sep = ''), each = n))

    p <- ggplot(df, aes(x = Time, y = Value, group = Coordinate,
                        color = Coordinate)) +
        geom_point(size = 2) + geom_path(size = 1) +
        theme_bw() + ggtitle(label) + ylab('') +
        scale_color_manual('', values = c('#e41a1c', '#377eb8', '#4daf4a', '#984ea3',
                                          '#ff7f00', '#ffff33', '#a65628', '#f781bf')) +
        theme(legend.position = 'bottom',
              legend.text = element_text(size = 14),
              axis.text = element_text(size = 14),
              axis.title = element_text(size = 16),
              title = element_text(size = 14))
    return(p)
}
